// Aidins sends aiding DVL data to the INS, it does not interact with the
// INS in any other way. The aiding data is read from a NATS Streaming
// Server
package main

import (
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	ins "bitbucket.org/uwaploe/go-ins"
	stan "github.com/nats-io/go-nats-streaming"
	"github.com/vmihailenco/msgpack"
)

var Version = "dev"
var BuildDate = "unknown"

func main() {
	natsURL := os.Getenv("NATS_URL")
	clusterID := os.Getenv("NATS_CLUSTER_ID")
	insAddr := os.Getenv("INS_ADDR")
	subject := os.Getenv("AIDING_SUBJECT")

	if insAddr == "" {
		log.Fatal("INS address not specified, aborting")
	}

	if subject == "" {
		log.Fatal("Aiding Data subject not specified, aborting")
	}

	conn, err := ins.NewConn(insAddr, time.Second*5)
	if err != nil {
		log.Fatalf("open connection: %v", err)
	}
	defer conn.Close()

	sc, err := stan.Connect(clusterID, "ins-aid", stan.NatsURL(natsURL))
	if err != nil {
		log.Fatalf("Cannot connect: %v", err)
	}
	defer sc.Close()

	dev := ins.NewDevice(conn)

	mcb := func(m *stan.Msg) {
		var rec ins.DvlData
		err := msgpack.Unmarshal(m.Data, &rec)
		if err != nil {
			log.Printf("Msgpack decode error: %v", err)
		} else {
			err = dev.SendDvl(rec)
			if err != nil {
				log.Printf("Error sending aiding data: %v", err)
			}
		}
	}
	sub, err := sc.Subscribe(subject, mcb, stan.StartWithLastReceived())
	if err != nil {
		log.Fatalf("Cannot subscribe to aiding data: %v", err)
	}
	log.Println("Subscribed to aiding data")
	defer sub.Unsubscribe()

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	log.Printf("INS Aiding Data Provider starting: %s", Version)

	// Exit on a signal
	<-sigs
}

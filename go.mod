module bitbucket.org/uwaploe/aidins

require (
	bitbucket.org/uwaploe/go-ins v1.1.2
	github.com/gogo/protobuf v1.2.1 // indirect
	github.com/nats-io/go-nats v1.7.2 // indirect
	github.com/nats-io/go-nats-streaming v0.4.4
	github.com/nats-io/nkeys v0.1.0 // indirect
	github.com/nats-io/nuid v1.0.1 // indirect
	github.com/vmihailenco/msgpack v4.0.4+incompatible
)
